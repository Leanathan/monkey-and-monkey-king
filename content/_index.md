---
title: 0
ishome: true
---

Salut, nous c'est Monkey et Nathanaël; deux singes connus sous le nom de *Monkey* et *Monkey King*. Nous sommes partis au Canada et avons décidé de partager avec vous une partie de nos aventures. Aucun de nous deux n'est un grand écrivain, mais nous avons chacun notre façon de raconter des histoires: pour Nathanaël c'est le gribouillage et pour Monkey c'est la photographie. Nous espérons que vous prendrez autant plaisir à regarder nos images que nous prenons à les produire.


Nos derniers méfaits retranscrits : 

[épisode 1 : L'avion](/1/)
